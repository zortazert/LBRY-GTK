################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, setproctitle, json, threading, sys

setproctitle.setproctitle("LBRY-GTK")

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GdkPixbuf, Gdk

from flbry import connect
from Source import Places, Settings
from Source.Page import Page
from Source.TopPanel import TopPanel
from Source.SidePanel import SidePanel

if sys.platform == "win32":
    MouseBack = 4
    MouseForward = 5
else:
    MouseBack = 8
    MouseForward = 9


class Handler:
    WindowHeight, WindowWidth = 0, 0

    def __init__(self, args):

        self.Builder = args

        self.Window = self.Builder.get_object("Window")
        self.Pager = self.Builder.get_object("Pager")
        self.Header = self.Builder.get_object("Header")
        self.TopPanelBox = self.Builder.get_object("TopPanelBox")
        self.SidePanelBox = self.Builder.get_object("SidePanelBox")

        self.Window.drag_dest_set(
            Gtk.DestDefaults.ALL,
            [Gtk.TargetEntry.new("text/plain", Gtk.TargetFlags.OTHER_APP, 0)],
            Gdk.DragAction.COPY,
        )

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "TopPanel.glade")

        with open(Places.ImageDir + "lbry-gtk.svg", "r") as File:
            Svg = File.read()
        Loader = GdkPixbuf.PixbufLoader()
        Loader.write(Svg.encode())
        Loader.close()
        self.Logo = Loader.get_pixbuf()

        self.TopPaneler = TopPanel(
            Builder,
            self.Window,
            self.Logo,
            self.Builder.get_object("BackImage"),
            self.NewPage,
            self.SamePage,
        )
        Builder.connect_signals(self.TopPaneler)
        self.TopPanelBox.pack_start(self.TopPaneler.TopPanel, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "SidePanel.glade")
        self.SidePaneler = SidePanel(
            Builder,
            self.Window,
            self.NewPage,
            self.SamePage,
        )
        Builder.connect_signals(self.SidePaneler)
        self.SidePanelBox.pack_start(self.SidePaneler.SidePanel, True, True, 0)

        self.Pages = []

        CssProvider = Gtk.CssProvider.new()
        CssProvider.load_from_data(b"* {padding: 0px;}")
        StyleContext = self.Header.get_style_context()
        StyleContext.add_provider(
            CssProvider, Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
        )
        self.on_Add_clicked()
        self.ExitQueue = self.Pages[-1].ExitQueue
        self.on_Header_switch_page(self.Header, self.Pages[-1].Page, 0)
        threading.Thread(
            target=self.Pages[0].Startuper.StartupHelper, args=([])
        ).start()

        self.TopPaneler.Startuper = self.Pages[0].Startuper
        self.SidePaneler.Startuper = self.Pages[0].Startuper

        self.Window.set_icon_from_file(Places.ImageDir + "lbry-gtk.svg")

    def NewPage(self, Function, Data):
        Page = self.Pages[self.Pager.get_current_page()]
        self.on_Add_clicked(".", Page.Stater.Export(Function, Data))

    def SamePage(self, Function, Data):
        Page = self.Pages[self.Pager.get_current_page()]
        Page.Stater.Import(Function, Data)

    def on_Back_clicked(self, Widget):
        Page = self.Pages[self.Pager.get_current_page()]
        threading.Thread(target=Page.Stater.Back, args=()).start()

    def on_Forward_clicked(self, button):
        Page = self.Pages[self.Pager.get_current_page()]
        threading.Thread(target=Page.Stater.Forward, args=()).start()

    def on_MenuItem_button_press_event(self, Widget, Event, State):
        Page = self.Pages[self.Pager.get_current_page()]
        if Event.button == Gdk.BUTTON_MIDDLE:
            self.on_Add_clicked(
                ".", Page.Stater.Export(State["Function"], State["Data"])
            )
        else:
            Page.Stater.Goto(State["Index"])

    def on_Window_button_press_event(self, Widget, Event):
        Page = self.Pages[self.Pager.get_current_page()]
        if (
            Event.button == MouseBack or Event.button == MouseForward
        ) and Page.Startuper.Started:
            LBRYSettings = Settings.Get()["preferences"]["shared"]["value"]
            if isinstance(LBRYSettings, str):
                Error(LBRYSettings, self.Window)
                return
            if LBRYSettings["LBRY-GTK"]["MouseBackAndForward"]:
                if Event.button == MouseBack:
                    self.on_Back_clicked("")
                elif Event.button == MouseForward:
                    self.on_Forward_clicked("")

    def on_Window_drag_data_received(
        self, widget, Context, X, Y, Data, Info, Time
    ):
        Page = self.Pages[self.Pager.get_current_page()]
        if Page.Startuper.Started:
            threading.Thread(
                target=Page.Publicationer.GetPublication,
                args=([Data.get_text()]),
            ).start()
        else:
            Error("LBRYNet is not running.", self.Window)

    def on_StaterList_button_press_event(self, Widget, Event):
        Page = self.Pages[self.Pager.get_current_page()]
        if Event.button == Gdk.BUTTON_SECONDARY:
            if Widget.get_name() == "Forward":
                List = Page.Stater.After()
            else:
                List = Page.Stater.Before()
            Length = len(List)
            if Length != 0:
                Menu = Gtk.Menu.new()
                for Index in range(Length):
                    Item = List[Index]
                    MenuItem = Gtk.MenuItem.new_with_label(Item["Title"])
                    MenuItem.connect(
                        "button-press-event",
                        self.on_MenuItem_button_press_event,
                        Item,
                    )
                    if Widget.get_name() == "Forward":
                        Menu.attach(MenuItem, 0, 1, Index, Index + 1)
                    else:
                        Menu.attach(
                            MenuItem, 0, 1, Length - Index - 1, Length - Index
                        )
                Menu.show_all()
                Menu.popup_at_widget(
                    Widget, Gdk.Gravity.SOUTH_WEST, Gdk.Gravity.NORTH_WEST
                )
        elif Event.button == Gdk.BUTTON_MIDDLE:
            if Widget.get_name() == "Forward":
                self.on_Add_clicked(".", Page.Stater.Next())
            else:
                self.on_Add_clicked(".", Page.Stater.Previous())

    def on_Add_clicked(self, Widget="", State=""):
        if Widget == "" or self.Pages[0].Startuper.Started:
            Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Page.glade")
            FirstPage = State
            try:
                with open(Places.ConfigDir + "Session.json", "r") as File:
                    Session = json.load(File)
            except:
                pass

            try:
                LBRYSettings = Settings.Get()
                if (
                    not isinstance(LBRYSettings, str)
                    and Widget != ""
                    and State == ""
                ):
                    Shared = LBRYSettings["preferences"]["shared"]["value"]
                    FirstPage = [
                        Shared["LBRY-GTK"]["HomeFunction"],
                        Shared["LBRY-GTK"]["HomeData"],
                    ]
            except:
                pass

            NoStartup = True
            if Widget == "":
                NoStartup = False

            self.Pages.append(
                Page(
                    Builder,
                    self.Window,
                    FirstPage,
                    NoStartup,
                    self.on_Add_clicked,
                    self.TopPaneler.Balance,
                )
            )
            Builder.connect_signals(self.Pages[-1])
            TitleBox = Gtk.Box.new(Gtk.Orientation.HORIZONTAL, 0)
            Width = 128
            try:
                Width = Session["PageWidth"]
            except:
                pass
            self.Pages[-1].Title.set_size_request(Width, -1)
            TitleBox.pack_start(self.Pages[-1].Title, False, False, 0)
            self.Pages[-1].Title.connect(
                "draw",
                self.on_Title_draw,
                self.Pages[-1],
            )
            CloseButton = Gtk.Button.new_from_icon_name(
                "window-close", Gtk.IconSize.BUTTON
            )
            CloseButton.connect(
                "clicked",
                self.on_CloseButton_clicked,
                self.Pages[-1],
            )
            TitleBox.pack_start(CloseButton, True, True, 0)
            CloseButton.set_halign(Gtk.Align.END)
            Expand = True
            try:
                Expand = Session["PageExpand"]
            except:
                pass
            CloseButton.set_hexpand(Expand)
            TitleBox.show_all()
            self.Pager.append_page(self.Pages[-1].MainSpace)
            self.Header.append_page(
                Gtk.Box.new(Gtk.Orientation.VERTICAL, 0), TitleBox
            )
            self.Pager.set_current_page(-1)
            self.Header.show_all()
            self.Header.set_current_page(-1)

    def on_Window_configure_event(self, Widget, Event):
        if self.WindowHeight != Event.height or self.WindowWidth != Event.width:
            self.Pages[self.Pager.get_current_page()].ShowHider.Hide()
            self.WindowHeight = Event.height
            self.WindowWidth = Event.width

    def on_Refresh_clicked(self, Widget):
        Page = self.Pages[self.Pager.get_current_page()]
        Page.Stater.Goto(Page.Stater.CurrentState)

    def on_Title_draw(self, Widget, Discard, Page):
        if Widget.get_name() != Widget.get_text():
            Widget.set_name(Widget.get_text())
            self.on_Header_switch_page(
                self.Header, Page, self.Pages.index(Page)
            )

    def on_CloseButton_clicked(self, Widget, Page):
        if len(self.Pages) != 1:
            Page.Publicationer.Queue.put(True)
            self.Header.remove_page(self.Pager.page_num(Page.MainSpace))
            self.Pager.remove_page(self.Pager.page_num(Page.MainSpace))
            self.Pages.remove(Page)

    def on_Header_switch_page(self, Widget, Page, Index):
        Text = (
            Widget.get_tab_label(Widget.get_nth_page(Index))
            .get_children()[0]
            .get_text()
        )
        if Text == "":
            Text = "LBRY-GTK"
        else:
            Text += " - LBRY-GTK"
        self.Window.set_title(Text)
        self.Pager.set_current_page(Index)

    def on_Window_destroy(self, *args):
        for Page in self.Pages:
            Page.Publicationer.Queue.put(True)
        self.ExitQueue.put(True)
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        if Session["Stop"]:
            connect.stop(server=Session["Server"])
        Gtk.main_quit()


App = Gtk.Builder.new_from_file(Places.GladeDir + "LBRY-GTK.glade")

App.connect_signals(Handler(App))

Gtk.main()

"""
settings.install_desktop(False)
settings.install_desktop(True)

        if " " in command:
            plugin.manager(command[command.find(" ")+1:])
        else:
            plugin.manager()

        wallet.addresses()
        wallet.address_send()
        donate.check_devs_file(save_changes=True)
        donate.check_devs_file(user_check=True)
        donate.check_devs_file(user_check=True, diff=True)
        donate.add()
        donate.donate()
        analytics.sales()

        if " " in command:
            plugin.get_plugin(command[command.find(" ")+1:])
        else:
            plugin.get_plugin()

        command = plugin.run(command, command, "main")

                    if " " in command:
            comments.inbox(command[command.find(" ")+1:])
        else:
            comments.inbox()

"""
