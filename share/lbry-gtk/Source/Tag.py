################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, Pango


class Tag:
    def __init__(self, *args):
        self.Builder, self.Edit, self.Tags = [*args, []]
        self.Entry = self.Builder.get_object("Entry")
        self.FlowBox = self.Builder.get_object("FlowBox")
        self.Tag = self.Builder.get_object("Tag")
        if self.Edit:
            self.Builder.get_object("Search").hide()
        else:
            self.Entry.hide()
            self.Builder.get_object("Add").hide()
            self.Builder.get_object("Remove").hide()

    def Add(self, Text):
        if Text != "" and not Text in self.Tags:
            self.FlowBox.add(
                Gtk.Label(
                    label=Text, wrap=True, wrap_mode=Pango.WrapMode.WORD_CHAR
                )
            )
            self.Entry.set_text("")
            self.FlowBox.show_all()
            self.Tags.append(Text)

    def Append(self, List):
        for Text in List:
            self.Add(Text)

    def on_Add_clicked(self, Widget=""):
        self.Add(self.Entry.get_text())

    def on_Entry_key_press_event(self, Widget, Event):
        if Gdk.keyval_name(Event.keyval) == "Return":
            self.on_Add_clicked()

    def on_FlowBox_key_press_event(self, Widget, Event):
        Key = Gdk.keyval_name(Event.keyval)
        if self.Edit and (Key == "Delete" or Key == "BackSpace"):
            self.on_Remove_clicked()

    def on_Remove_clicked(self, Widget=""):
        SelectedChildren = self.FlowBox.get_selected_children()
        for Child in SelectedChildren:
            self.Tags.remove(Child.get_children()[0].get_label())
            self.FlowBox.remove(Child)

    def on_SelectAll_clicked(self, Widget=""):
        self.FlowBox.select_all()

    def RemoveAll(self):
        self.on_SelectAll_clicked()
        self.on_Remove_clicked()

    def on_Search_clicked(self, Widget):
        TagLabels = self.FlowBox.get_selected_children()
        if len(TagLabels) != 0:
            Tags, NewTitle = [], "Tagsearch:"
            for TagLabel in TagLabels:
                Tags.append(TagLabel.get_child().get_label())
                NewTitle = NewTitle + " " + TagLabel.get_child().get_label()
            threading.Thread(
                target=self.ButtonThread,
                args=(["Search", "Content", NewTitle, {"tags": Tags}]),
            ).start()

    def on_Unselect_clicked(self, Widget):
        self.FlowBox.unselect_all()
