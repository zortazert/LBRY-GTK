################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, queue, copy, datetime

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Gdk, GLib

from Source import ShowHideHelper, Places, Settings, ListCreate
from Source.Replace import Replace
from Source.ShowHide import ShowHide
from Source.Publication import Publication
from Source.NewPublication import NewPublication
from Source.List import List
from Source.Error import Error
from Source.State import State
from Source.DateTime import DateTime
from Source.Order import Order
from Source.Tag import Tag
from Source.StateThread import StateThread
from Source.Startup import Startup


class Page:
    MainSpaceHeight, MainSpaceWidth = 0, 0
    ExitQueue = queue.Queue()

    def __init__(self, *Args):
        self.ShowHiderTexts = []
        self.Stater = State()
        (
            self.Builder,
            self.Window,
            FirstPage,
            NoStartup,
            self.AddPage,
            self.Balance,
        ) = Args
        self.Page = self.Builder.get_object("Page")
        self.Title = self.Builder.get_object("Title")
        self.Title.set_text("")
        self.MainSpace = self.Builder.get_object("MainSpace")
        Spaces = {
            "Content": self.Builder.get_object("ContentSpace"),
            "Wallet": self.Builder.get_object("WalletSpace"),
            "File": self.Builder.get_object("FileSpace"),
            "Notification": self.Builder.get_object("NotificationSpace"),
        }

        self.DateTimeBox = self.Builder.get_object("DateTimeBox")
        self.OrderBox = self.Builder.get_object("OrderBox")
        self.Inequality = self.Builder.get_object("Inequality")
        self.Text = self.Builder.get_object("Text")
        self.Channel = self.Builder.get_object("Channel")
        self.ClaimType = self.Builder.get_object("ClaimType")
        self.AnyTagBox = self.Builder.get_object("AnyTagBox")
        self.AllTagBox = self.Builder.get_object("AllTagBox")
        self.NotTagBox = self.Builder.get_object("NotTagBox")
        self.ClaimIdBox = self.Builder.get_object("ClaimIdBox")
        self.ChannelIdBox = self.Builder.get_object("ChannelIdBox")
        self.StreamTypeBox = self.Builder.get_object("StreamTypeBox")
        self.StreamType = self.Builder.get_object("StreamType")
        self.DateType = self.Builder.get_object("DateType")
        self.DateLabel = self.Builder.get_object("DateLabel")
        self.DateInterval = self.Builder.get_object("DateInterval")
        self.DateValue = self.Builder.get_object("DateValue")
        self.ContentView = self.Builder.get_object("ContentView")

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.AnyTagger = Tag(Builder, True)
        Builder.connect_signals(self.AnyTagger)
        self.AnyTagBox.pack_start(self.AnyTagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.AllTagger = Tag(Builder, True)
        Builder.connect_signals(self.AllTagger)
        self.AllTagBox.pack_start(self.AllTagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.NotTagger = Tag(Builder, True)
        Builder.connect_signals(self.NotTagger)
        self.NotTagBox.pack_start(self.NotTagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.ClaimIdTagger = Tag(Builder, True)
        Builder.connect_signals(self.ClaimIdTagger)
        self.ClaimIdBox.pack_start(self.ClaimIdTagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.ChannelIdTagger = Tag(Builder, True)
        Builder.connect_signals(self.ChannelIdTagger)
        self.ChannelIdBox.pack_start(self.ChannelIdTagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.StreamTyper = Tag(Builder, True)
        Builder.connect_signals(self.StreamTyper)
        self.StreamTypeBox.pack_start(self.StreamTyper.Tag, True, True, 0)
        self.StreamTyper.Entry.hide()
        self.on_StreamType_changed(self.StreamType)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "DateTime.glade")
        self.DateTimer = DateTime(Builder)
        Builder.connect_signals(self.DateTimer)
        self.DateTimeBox.pack_start(self.DateTimer.DateTime, True, True, 0)
        self.DateTimer.DateTime.set_no_show_all(True)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Order.glade")
        self.Orderer = Order(Builder)
        Builder.connect_signals(self.Orderer)
        self.OrderBox.pack_start(self.Orderer.Order, True, True, 0)

        self.on_DateType_changed(self.DateType)

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "Publication.glade"
        )
        self.Publicationer = Publication(
            Builder,
            self.ShowHiderTexts,
            self.Window,
            self.Stater,
            self.Title,
            self.MainSpace,
            self.AddPage,
        )
        Builder.connect_signals(self.Publicationer)

        Builder = Gtk.Builder.new_from_file(
            Places.GladeDir + "NewPublication.glade"
        )
        self.NewPublicationer = NewPublication(Builder, self.Window, self.Title)
        Builder.connect_signals(self.NewPublicationer)

        self.ShowHider = ShowHide(
            [
                [
                    self.ShowHiderTexts,
                    ShowHideHelper.CommentShow,
                    ShowHideHelper.CommentHide,
                ],
            ],
            200,
        )

        ListCreate.MakeColumns(self.ContentView, self.Publicationer.DataView)

        self.Lister = List(
            self.ContentView,
            self.Window,
            self.Publicationer,
            {
                "Row": self.Builder.get_object("RowStore"),
                "Data": self.Builder.get_object("DataStore"),
            },
            [
                self.Builder.get_object("Total"),
                self.Builder.get_object("Available"),
                self.Builder.get_object("Reserved"),
                self.Builder.get_object("Claims"),
                self.Builder.get_object("Supports"),
                self.Builder.get_object("Tips"),
            ],
            self.MainSpace,
            self.Stater,
            self.Builder.get_object("Grid"),
            Spaces,
            self.Text,
            self.Orderer,
            self.Channel,
            self.ClaimType,
            self.AnyTagger,
            self.AllTagger,
            self.NotTagger,
            self.ClaimIdTagger,
            self.ChannelIdTagger,
            self.DateTimer,
            self.Inequality,
            self.Builder.get_object("Advanced"),
            self.StreamTyper,
            self.DateType,
            self.Title,
            self.AddPage,
        )
        self.Publicationer.PublicationControler.ButtonThread = (
            self.Lister.ButtonThread
        )
        self.Publicationer.Tagger.ButtonThread = self.Lister.ButtonThread

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Startup.glade")
        self.Startuper = Startup(
            Builder,
            self.Window,
            self.Balance,
            self.ExitQueue,
            self.Stater,
            self.Title,
        )
        Builder.connect_signals(self.Startuper)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Settings.glade")
        self.Settingser = Settings.Settings(
            Builder, self.Window, self.Title, self.Stater, self.Startuper
        )
        Builder.connect_signals(self.Settingser)

        self.StateThreader = StateThread(
            self.Stater,
            self.NewPublicationer,
            self.Publicationer,
            self.Settingser,
            self.Lister,
        )

        self.Replacer = Replace(
            {
                "Publication": self.Publicationer.Publication,
                "ContentList": Spaces["Content"],
                "WalletList": Spaces["Wallet"],
                "FileList": Spaces["File"],
                "NotificationList": Spaces["Notification"],
                "NewPublication": self.NewPublicationer.NewPublication,
                "Settings": self.Settingser.Settings,
                "Startup": self.Startuper.Startup,
                "Document": self.Publicationer.Documenter.Markdowner.Document,
            },
            self.MainSpace,
        )
        self.Publicationer.Replace = self.Replacer.Replace
        self.Publicationer.Documenter.Replace = self.Replacer.Replace
        self.NewPublicationer.Replace = self.Replacer.Replace
        self.Startuper.Replace = self.Replacer.Replace
        self.Settingser.Replace = self.Replacer.Replace
        self.Lister.Replace = self.Replacer.Replace
        self.Stater.PossibleStates = {
            "Following": self.StateThreader.FollowingThread,
            "YourTags": self.StateThreader.YourTagsThread,
            "Discover": self.StateThreader.DiscoverThread,
            "Library": self.StateThreader.LibraryThread,
            "Library Search": self.StateThreader.LibrarySearchThread,
            "Collections": self.StateThreader.CollectionsThread,
            "Followed": self.StateThreader.FollowedThread,
            "Uploads": self.StateThreader.UploadsThread,
            "Channels": self.StateThreader.ChannelsThread,
            "Advanced Search": self.Lister.ButtonThread,
            "Document": self.Publicationer.Documenter.Display,
            "Publication": self.Publicationer.GetPublication,
            "NewPublication": self.StateThreader.NewPublicationThread,
            "Settings": self.StateThreader.SettingsThread,
            "Help": self.StateThreader.HelpThread,
            "Status": self.Startuper.ShowStatusThread,
            "Home": self.StateThreader.HomeThread,
            "Inbox": self.StateThreader.InboxThread,
        }

        self.Startuper.Started = NoStartup

        self.Window.show_all()

        if not isinstance(FirstPage, str):
            self.Stater.Import(*FirstPage)

    def on_StreamType_changed(self, Widget):
        self.StreamTyper.Entry.set_text(Widget.get_active_text().lower())

    def on_SimpleDate_changed(self, Widget=""):
        self.Inequality.set_active(1)
        DateValue = self.DateValue.get_active()
        if self.DateInterval.get_active() == 0:
            self.DateTimer.on_DateReset_clicked()
            self.DateTimer.Second.set_value(0)
            if -1 < DateValue:
                self.DateTimer.Minute.set_value(0)
            if 0 < DateValue:
                self.DateTimer.Hour.set_value(0)
            Date = datetime.date.today()
            if 1 < DateValue:
                Date -= datetime.timedelta(days=Date.weekday())
            if 2 < DateValue:
                Date -= datetime.timedelta(days=Date.day - 1)
            if 3 < DateValue:
                Date = datetime.date(Date.year, 1, 1)
            self.DateTimer.Date.select_day(Date.day)
            self.DateTimer.Date.select_month(Date.month - 1, Date.year)
        else:
            self.DateTimer.on_DateReset_clicked()
            if 0 == DateValue:
                self.DateTimer.Hour.spin(Gtk.SpinType.STEP_BACKWARD, 1)
            Date = datetime.date.today()
            if 1 == DateValue:
                Date -= datetime.timedelta(days=1)
            if 2 == DateValue:
                Date -= datetime.timedelta(days=7)
            if 3 == DateValue:
                if Date.month == 1:
                    Date = datetime.date(Date.year, 12, Date.day)
                else:
                    Date = datetime.date(Date.year, Date.month - 1, Date.day)
            if 4 == DateValue:
                Date = datetime.date(Date.year - 1, Date.month, Date.day)
            self.DateTimer.Date.select_day(Date.day)
            self.DateTimer.Date.select_month(Date.month - 1, Date.year)

    def on_DateType_changed(self, Widget):
        Children = self.DateTimeBox.get_children()
        self.DateTimer.DateUse.set_active(Widget.get_active() == 1)
        self.DateLabel.set_visible(Widget.get_active() != 0)
        if Widget.get_active() == 0:
            for Child in Children:
                Child.hide()
        elif Widget.get_active() == 1:
            Children[0].show()
            Children[1].hide()
            Children[2].hide()
            self.on_SimpleDate_changed()
        elif Widget.get_active() == 2:
            self.Inequality.set_active(0)
            self.DateTimer.on_DateReset_clicked()
            Children[0].hide()
            Children[1].show()
            Children[2].show()

    def on_ContentView_button_press_event(self, Widget, Event):
        Store, Path = Widget.get_selection().get_selected_rows()
        Url = [Store[Path][-4]]
        if Url != [""]:
            if Event.button == Gdk.BUTTON_PRIMARY:
                threading.Thread(
                    target=self.Publicationer.GetPublication, args=(Url)
                ).start()
            elif Event.button == Gdk.BUTTON_MIDDLE:
                self.AddPage(".", ["Publication", Url])

    def on_MainSpace_draw(self, Widget, event):
        self.Publicationer.Thumbnailer.Thumbnail.queue_draw()
        OldHeight = self.MainSpaceHeight
        OldWidth = self.MainSpaceWidth
        self.MainSpaceHeight = self.MainSpace.get_allocated_height()
        self.MainSpaceWidth = self.MainSpace.get_allocated_width()
        Space = self.Replacer.GetSpace()
        if (
            (OldHeight < self.MainSpaceHeight or OldWidth < self.MainSpaceWidth)
            and Space.endswith("List")
            and self.Lister.Started
        ):
            LBRYSettings = Settings.Get()
            if isinstance(LBRYSettings, str):
                Error(self.Window, LBRYSettings)
                return
            threading.Thread(
                target=self.Lister.GenericLoaded, args=([LBRYSettings])
            ).start()

    def on_MainSpace_edge_overshot(self, Widget, Position):
        self.on_MainSpace_edge_reached(Widget, Position)

    def on_MainSpace_edge_reached(self, Widget, Position):
        Space = self.Replacer.GetSpace()
        if Space.endswith("List") and Position == Gtk.PositionType.BOTTOM:
            LBRYSettings = Settings.Get()
            if isinstance(LBRYSettings, str):
                Error(self.Window, LBRYSettings)
                return
            threading.Thread(
                target=self.Lister.UpdateMainSpace, args=([LBRYSettings, 1])
            ).start()

    def on_Search_button_press_event(self, Widget, Event):
        if Event.button == Gdk.BUTTON_PRIMARY:
            threading.Thread(
                target=self.StateThreader.LibrarySearchThread,
                args=([]),
            ).start()
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.AddPage(".", ["Library Search", []])

    def on_Apply_button_press_event(self, Widget, Event):
        RawTime = self.DateTimer.GetTime()
        Time = str(RawTime)
        TimeText = "equals "
        if Time != "-1":
            Inequality = self.Inequality.get_active()
            if Inequality == 0:
                TimeText = "less than "
                Time = "<" + Time
            elif Inequality == 1:
                TimeText = "more than "
                Time = ">" + Time
            elif Inequality == 3:
                TimeText = "less or equals "
                Time = "<=" + Time
            elif Inequality == 4:
                TimeText = "more or equals "
                Time = ">=" + Time

        Data = {
            "text": self.Text.get_text(),
            "timestamp": Time,
            "channel": self.Channel.get_text(),
            "any_tags": copy.deepcopy(self.AnyTagger.Tags),
            "all_tags": copy.deepcopy(self.AllTagger.Tags),
            "not_tags": copy.deepcopy(self.NotTagger.Tags),
            "channel_ids": copy.deepcopy(self.ChannelIdTagger.Tags),
            "claim_ids": copy.deepcopy(self.ClaimIdTagger.Tags),
            "stream_types": copy.deepcopy(self.StreamTyper.Tags),
        }

        Order = self.Orderer.Get()
        if Order:
            Data["order_by"] = [Order]
        else:
            Data["order_by"] = []

        ClaimType = self.ClaimType.get_active_text().lower()

        if ClaimType != "all":
            Data["claim_type"] = ClaimType

        Title = ""
        if Data["text"] != "":
            Title = "Text: " + Data["text"] + ", "
        if Data["timestamp"] != "-1":
            Title += (
                "Time: "
                + TimeText
                + str(datetime.datetime.fromtimestamp(RawTime))
                + ", "
            )
        if "order_by" in Data:
            Title += self.Orderer.Print()
        if "claim_type" in Data:
            Title += "Type: " + Data["claim_type"] + ", "
        if Data["stream_types"] != []:
            Title += "Stream Types: " + ", ".join(Data["stream_types"]) + ", "
        if Data["any_tags"] != []:
            Title += "Any tags: " + ", ".join(Data["any_tags"]) + ", "
        if Data["all_tags"] != []:
            Title += "All tags: " + ", ".join(Data["all_tags"]) + ", "
        if Data["not_tags"] != []:
            Title += "Not tags: " + ", ".join(Data["not_tags"]) + ", "
        if Data["channel"] != "":
            Title += "Channel: " + Data["channel"] + ", "
        if Data["channel_ids"] != []:
            Title += "Channels: " + ", ".join(Data["channel_ids"]) + ", "
        if Data["claim_ids"] != []:
            Title += "Claims: " + ", ".join(Data["claim_ids"]) + ", "

        Title = "Advanced Search - " + Title[:-2]

        if Event.button == Gdk.BUTTON_PRIMARY:
            threading.Thread(
                target=self.Lister.ButtonThread,
                args=(["Search", "Content", Title, Data]),
            ).start()
        elif Event.button == Gdk.BUTTON_MIDDLE:
            self.AddPage(
                ".", ["Advanced Search", ["Search", "Content", Title, Data]]
            )
