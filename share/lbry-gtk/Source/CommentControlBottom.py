################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import threading, json, gi

gi.require_version("Gtk", "3.0")
from gi.repository import GLib, Gtk

from flbry import comments, support

from Source import Places
from Source.Error import Error


class CommentControlBottom:
    def __init__(self, *args):
        (
            self.Builder,
            self.Window,
            self.Row,
            self.Channelser,
            self.Markdowns,
            self.Box,
            self.ClaimID,
            self.CommentServer,
            self.ChannelList,
            self.SingleComment,
            self.Boxes,
            self.Settings,
            self.RepliesFrame,
            self.RepliesBox,
            self.RepliesExpander,
            self.HiddenUI,
            self.PChannel,
            self.ReplyText,
            self.Promo,
            self.on_ReplyText_key_release_event,
            self.CommentExpander,
        ) = args
        self.CommentID = self.Row[4]
        self.CommentControlBottom = self.Builder.get_object(
            "CommentControlBottom"
        )
        self.ChannelsBox2 = self.Builder.get_object("ChannelsBox2")
        self.Post = self.Builder.get_object("Post")
        self.Save = self.Builder.get_object("Save")
        self.Tip = self.Builder.get_object("Tip")
        self.TipLabel1 = self.Builder.get_object("TipLabel1")
        self.TipLabel2 = self.Builder.get_object("TipLabel2")

    def on_Save_clicked(self, Widget, Discard=""):
        for ChannelItem in self.ChannelList:
            if ChannelItem[0] == self.Row[2]:
                Channel = [ChannelItem[0], ChannelItem[-1]]
                break
        TextBuffer = self.ReplyText.get_buffer()
        StartIter = TextBuffer.get_start_iter()
        EndIter = TextBuffer.get_end_iter()
        Text = TextBuffer.get_text(StartIter, EndIter, True)
        args = [Text, Channel]
        threading.Thread(target=self.SaveThread, args=(args)).start()

    def SaveThread(self, Text, Channel):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        ErrorOrData = comments.update(
            Text,
            self.CommentID,
            *Channel,
            self.CommentServer,
            server=Session["Server"],
        )
        if isinstance(ErrorOrData, str):
            Error(ErrorOrData, self.Window)
            return
        self.Row[3] = Text
        self.Markdowns[0]["Markdowner"].Text = Text
        GLib.idle_add(self.SaveUpdate)

    def SaveUpdate(self):
        self.Markdowns[0]["Markdowner"].Fill()
        self.on_Edit_clicked(self.Edit)

    def on_Post_clicked(self, Widget):
        ChannelRow = self.Channelser.Get()
        TextBuffer = self.ReplyText.get_buffer()
        Start = TextBuffer.get_start_iter()
        End = TextBuffer.get_end_iter()
        Text, Channel = TextBuffer.get_text(Start, End, False), [
            ChannelRow[0],
            ChannelRow[-1],
        ]
        if Text != "":
            Label = self.CommentExpander.get_label()
            Tip = self.Tip.get_value()
            if Label != "Comments":
                MinimumTip = float(Label.split(" ")[2])
                if Tip < MinimumTip:
                    Error(Label[10:-1], self.Window)
                    return
            if Tip != 0:
                Dialog = Gtk.MessageDialog(buttons=Gtk.ButtonsType.OK_CANCEL)
                Dialog.props.text = (
                    "Are you sure you want to tip " + str(Tip) + " LBC?"
                )
                Response = Dialog.run()
                Dialog.destroy()
                if Response != Gtk.ResponseType.OK:
                    return
                else:
                    args = [Text, Channel, Widget, Tip]
                    threading.Thread(target=self.TipThread, args=(args)).start()
                    return
            args = [Text, Channel, Widget]
            threading.Thread(target=self.PostThread, args=(args)).start()

    def TipThread(self, Text, Channel, Widget, Tip):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        JsonData = support.create(
            self.ClaimID,
            Tip,
            True,
            *Channel,
            Session["Server"],
        )
        if isinstance(JsonData, str):
            Error(JsonData, self.Window)
        else:
            self.PostThread(
                Text, Channel, Widget, Tip, JsonData["outputs"][0]["txid"]
            )

    def PostThread(self, Text, Channel, Widget, Tip=0, TipID=""):
        if self.Settings["PromoteLBRYGTK"]:
            Text += self.Promo
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        ErrorOrData = comments.post(
            self.ClaimID,
            Text,
            *Channel,
            self.CommentServer,
            self.CommentID,
            support_tx_id=TipID,
            server=Session["Server"],
        )
        if isinstance(ErrorOrData, str):
            Error(ErrorOrData, self.Window)
            return
        GLib.idle_add(self.PostUpdate, Channel, Widget, Text, ErrorOrData, Tip)

    def PostUpdate(self, Channel, Widget, Text, Data, Tip):
        self.ReplyText.get_buffer().set_text("")
        if self.CommentID == "":
            Box = self.Box
        else:
            self.RepliesFrame.set_no_show_all(False)
            Box = self.RepliesBox
            self.RepliesExpander.set_label(
                "Replies (" + str(len(self.RepliesBox.get_children()) + 1) + ")"
            )
            self.RepliesFrame.show_all()
        Row = [
            0,
            Tip,
            Channel[0],
            Text,
            Data["comment_id"],
            self.Channelser.Get()[-1],
            self.Channelser.Get()[-2],
            0,
            0,
            False,
            False,
            False,
        ]
        self.SingleComment(
            self.ClaimID,
            Box,
            Row,
            self.Boxes,
            self.CommentServer,
            False,
            False,
            self.Settings,
            self.ChannelList,
            self.PChannel,
        )
        self.on_ReplyText_key_release_event(self.ReplyText)
        if not self.HiddenUI:
            self.on_Reply_clicked(self.Reply)
        Box.show_all()
