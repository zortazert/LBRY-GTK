################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, copy, pathlib, queue

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, Gdk

from flbry import settings, connect, channel, meta

from Source import Places, Startup
from Source.Error import Error
from Source.Channels import Channels
from Source.Tag import Tag
from Source.Order import Order


def Get():
    with open(Places.ConfigDir + "Session.json", "r") as File:
        Session = json.load(File)
    LBRYSettings = settings.get(server=Session["Server"])
    if isinstance(LBRYSettings, str):
        return LBRYSettings
    LBRYSettings["Session"] = Session
    return LBRYSettings


UpdateMetaQueue = queue.Queue()


def UpdateMeta(Inbox, Window):
    # Exit if user disabled Meta Service otherwise continue
    global UpdateMetaQueue
    threading.Thread(target=UpdateMetaThread, args=([Inbox, Window])).start()
    try:
        UpdateMetaQueue.get(block=False)
        return False
    except:
        return True


def UpdateMetaThread(Inbox, Window):
    # Check if user disabled Meta Service
    global UpdateMetaQueue
    with open(Places.ConfigDir + "Session.json", "r") as File:
        Session = json.load(File)
    if not Session["EnableMetaService"]:
        UpdateMetaQueue.put(True)
        return

    # Get Inbox
    LBRYSettings = Get()
    if isinstance(LBRYSettings, str):
        Error(LBRYSettings, Window)
        return
    LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"]["LBRY-GTK"]

    try:
        Notification = meta.list(
            LBRYGTKSettings["MetaServer"], LBRYGTKSettings["AuthToken"], True
        )[0]

        # Check if last notification is new
        with open(Places.CacheDir + "Notification.json", "r") as File:
            LastNotification = json.load(File)["LastNotification"]
        if LastNotification != Notification["id"]:
            GLib.idle_add(UpdateMetaUpdate, Inbox)
    except:
        pass


def UpdateMetaUpdate(Inbox):
    Inbox.set_active(True)


def ChangeMeta(Window, EnableMetaService):
    # Hide or show Icon
    TopPanel = (
        Window.get_children()[0]
        .get_children()[1]
        .get_children()[1]
        .get_children()[0]
    )
    Children = TopPanel.get_children()
    for Child in Children:
        try:
            if Child.get_label() == "🔔":
                Inbox = Child
                Child.set_visible(EnableMetaService)
                break
        except:
            pass

    # Start notification service
    if EnableMetaService:
        GLib.timeout_add(60 * 1000, UpdateMeta, Inbox, Window)
        threading.Thread(
            target=UpdateMetaThread, args=([Inbox, Window])
        ).start()


MinimumWidth = 0

MenuIcons = [
    "menu_new",
    "preferences-other",
    "image-loading",
    "document-properties",
    "applications-others",
]


def ChangeMenu(Window, Type, MenuIcon="", Remove=True):
    # Set MinimumWidth with full menu only once
    global MinimumWidth, MenuIcons
    if MinimumWidth == 0:
        MinimumWidth = Window.get_preferred_size().minimum_size.width

    TopPanel = (
        Window.get_children()[0]
        .get_children()[1]
        .get_children()[1]
        .get_children()[0]
    )

    Children = TopPanel.get_children()
    Menu = ""

    # Get all needed components
    for Child in Children:
        if isinstance(Child, Gtk.Box):
            try:
                Menu = Child.get_children()[0]
            except:
                pass
            MenuBox = Child
        if isinstance(Child, Gtk.MenuButton):
            Button = Child

    # Set icon
    Image = Button.get_children()[0]
    if MenuIcon != "" and Image.get_icon_name() != MenuIcons[MenuIcon]:
        Image.set_from_icon_name(MenuIcons[MenuIcon], Gtk.IconSize.BUTTON)

    # Do not change type if it's already that
    if TopPanel.get_name() != str(Type):
        TopPanel.set_name(str(Type))

        Popover = Button.get_popover()

        if Menu == "":
            Menu = Popover.get_children()[0]

        # Initial settings needed for each type
        Menu.unparent()
        MenuBox.foreach(MenuBox.remove)
        Popover.foreach(Popover.remove)
        Button.set_visible(Type == 2)
        WindowName = Window.get_name()

        # Do not remove function, if original type is adaptive
        if WindowName != "GtkWindow" and Remove:
            Window.disconnect(int(WindowName))
            Window.set_name("GtkWindow")

        # Add menu to the correct place
        # Adaptive adds it to the button, as function assumes it is somewhere
        if Type == 0:
            Window.set_name(str(Window.connect("configure-event", UpdateMenu)))
            Menu.set_pack_direction(Gtk.PackDirection.TTB)
            Popover.add(Menu)
            UpdateMenu(Window, "", Window.get_size().width)
        elif Type == 1:
            MenuBox.add(Menu)
            Menu.set_pack_direction(Gtk.PackDirection.LTR)
        elif Type == 2:
            Menu.set_pack_direction(Gtk.PackDirection.TTB)
            Popover.add(Menu)


def UpdateMenu(Window, Event, Width=""):
    # Setting MenuType depending on Window size
    if Width == "":
        Width = Event.width
    global MinimumWidth
    if Width < MinimumWidth:
        ChangeMenu(Window, 2, "", False)
    else:
        ChangeMenu(Window, 1, "", False)


class Settings:
    def __init__(self, *args):
        (
            self.Builder,
            self.Window,
            self.Title,
            self.Stater,
            self.Startuper,
        ) = args
        Builder = self.Builder
        self.Settings = Builder.get_object("Settings")

        self.SaveFiles = Builder.get_object("SaveFiles")
        self.SaveBlobs = Builder.get_object("SaveBlobs")
        self.SaveResolved = Builder.get_object("SaveResolved")
        self.DownloadPath = Builder.get_object("DownloadPath")
        self.MatureContent = Builder.get_object("MatureContent")
        self.BlobDownloadTimeout = Builder.get_object("BlobDownloadTimeout")
        self.DownloadTimeout = Builder.get_object("DownloadTimeout")
        self.HubTimeout = Builder.get_object("HubTimeout")
        self.NodeRPCTimeout = Builder.get_object("NodeRPCTimeout")
        self.PeerConnectTimeout = Builder.get_object("PeerConnectTimeout")
        self.LocalTagBox = Builder.get_object("LocalTagBox")
        self.SharedTagBox = Builder.get_object("SharedTagBox")
        self.NotTagBox = Builder.get_object("NotTagBox")
        self.OrderBox = Builder.get_object("OrderBox")
        self.CommentChannelBox = Builder.get_object("CommentChannelBox")
        self.PresetHome = Builder.get_object("PresetHome")

        self.SettingFields = {}
        for Setting in Startup.DefaultSettings:
            self.SettingFields[Setting] = Builder.get_object(Setting)

        self.SessionFields = {}
        for Setting in Startup.DefaultSession:
            self.SessionFields[Setting] = Builder.get_object(Setting)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.LocalTagger = Tag(Builder, True)
        Builder.connect_signals(self.LocalTagger)
        self.LocalTagBox.pack_start(self.LocalTagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.SharedTagger = Tag(Builder, True)
        Builder.connect_signals(self.SharedTagger)
        self.SharedTagBox.pack_start(self.SharedTagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.NotTagger = Tag(Builder, True)
        Builder.connect_signals(self.NotTagger)
        self.NotTagBox.pack_start(self.NotTagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Order.glade")
        self.Orderer = Order(Builder)
        Builder.connect_signals(self.Orderer)
        self.OrderBox.pack_start(self.Orderer.Order, True, True, 0)

    def GetValue(self, Widget):
        try:
            return Widget.get_value()
        except:
            try:
                return Widget.get_active()
            except:
                try:
                    return Widget.get_text()
                except:
                    pass

    def SetValue(self, Widget, Value):
        try:
            Widget.set_value(Value)
        except:
            try:
                Widget.set_active(Value)
            except:
                try:
                    Widget.set_text(Value)
                except:
                    pass

    def ShowSettings(self, OverWrite=True):
        if OverWrite:
            with open(Places.ConfigDir + "Session.json", "r") as File:
                self.Session = json.load(File)
        self.Loaded = False
        if self.Startuper.Started:
            if OverWrite:
                self.LBRYSetting = settings.get(server=self.Session["Server"])
            Settings = self.LBRYSetting["settings"]
            Shared = self.LBRYSetting["preferences"]["shared"]["value"]
            Local = self.LBRYSetting["preferences"]["local"]["value"]
            LBRYGTK = Shared["LBRY-GTK"]
            self.MatureContent.set_active(Shared["settings"]["show_mature"])
            self.SaveFiles.set_active(Settings["save_files"])
            self.SaveBlobs.set_active(Settings["save_blobs"])
            self.SaveResolved.set_active(Settings["save_resolved_claims"])
            self.DownloadPath.set_filename(Settings["download_dir"])
            self.BlobDownloadTimeout.set_value(
                Settings["blob_download_timeout"]
            )
            self.DownloadTimeout.set_value(Settings["download_timeout"])
            self.HubTimeout.set_value(Settings["hub_timeout"])
            self.NodeRPCTimeout.set_value(Settings["node_rpc_timeout"])
            self.PeerConnectTimeout.set_value(Settings["peer_connect_timeout"])
            self.LocalTagger.Append(Local["tags"])
            self.SharedTagger.Append(Shared["tags"])
            self.NotTagger.Append(LBRYGTK["NotTags"])

            for Setting in self.SettingFields:
                self.SetValue(self.SettingFields[Setting], LBRYGTK[Setting])

            self.Orderer.Set(LBRYGTK["Order"])

            self.ChannelsBuilder = Gtk.Builder.new_from_file(
                Places.GladeDir + "Channels.glade"
            )
            ChannelList = channel.channel_list(server=self.Session["Server"])
            self.Channelser = Channels(
                self.ChannelsBuilder, False, self.Window, ChannelList, LBRYGTK
            )
            self.ChannelsBuilder.connect_signals(self.Channelser)
            self.CommentChannel = self.ChannelsBuilder.get_object("Channels")
            self.CommentChannelBox.forall(self.CommentChannelBox.remove)
            self.CommentChannelBox.add(self.CommentChannel)
            self.on_HomeType_changed(self.SettingFields["HomeType"])
            self.Loaded = True

        for Setting in self.SessionFields:
            self.SetValue(self.SessionFields[Setting], self.Session[Setting])

        self.Replace("Settings")
        self.Title.set_text("Settings")

        self.SettingFields["AuthToken"].set_visibility(False)

    def on_Edit_clicked(self, Widget):
        self.SettingFields["AuthToken"].set_visibility(
            not self.SettingFields["AuthToken"].get_visibility()
        )

    States = [
        ["Following", "[]"],
        ["YourTags", "[]"],
        ["Discover", "[]"],
        ["Library", "[]"],
        ["Library Search", "[]"],
        ["Collections", "[]"],
        ["Followed", "[]"],
        ["Uploads", "[]"],
        ["Channels", "[]"],
        ["NewPublication", "[]"],
        ["Settings", "[]"],
        ["Status", "[]"],
        ["Help", "[]"],
        ["Inbox", "[]"],
    ]

    def on_HomeType_changed(self, Widget):
        Children = Widget.get_parent().get_children()
        if Widget.get_active() == 0:
            for Index in range(2, len(Children)):
                Children[Index].set_visible(Index < 4)
            for Index in range(len(self.States)):
                State = self.States[Index]
                if (
                    self.SettingFields["HomeFunction"].get_text() == State[0]
                    and self.SettingFields["HomeData"].get_text() == State[1]
                ):
                    self.PresetHome.set_active(Index)
                    break
            self.on_PresetHome_changed(self.PresetHome)
        else:
            for Index in range(2, len(Children)):
                Children[Index].set_visible(3 < Index)

    def on_MenuItem_activate(self, Widget, State):
        self.SettingFields["HomeFunction"].set_text(State[0])
        self.SettingFields["HomeData"].set_text(State[1])

    def on_HomeFromPageHistory_clicked(self, Widget):
        List = self.Stater.Before()
        Length = len(List)
        if Length != 0:
            Menu = Gtk.Menu.new()
            for Index in range(Length):
                Item = List[Index]
                MenuItem = Gtk.MenuItem.new_with_label(Item["Title"])
                MenuItem.connect(
                    "activate",
                    self.on_MenuItem_activate,
                    self.Stater.Export(
                        Item["Function"],
                        Item["Data"],
                        True,
                    ),
                )
                Menu.attach(MenuItem, 0, 1, Length - Index - 1, Length - Index)
            Menu.show_all()
            Menu.popup_at_widget(
                Widget, Gdk.Gravity.SOUTH_WEST, Gdk.Gravity.NORTH_WEST
            )

    def on_PresetHome_changed(self, Widget):
        self.on_MenuItem_activate("", self.States[Widget.get_active()])

    def on_Export_clicked(self, Widget):
        Dialog = Gtk.FileChooserDialog(
            "Export settings - LBRY-GTK",
            self.Window,
            Gtk.FileChooserAction.SAVE,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_SAVE,
                Gtk.ResponseType.ACCEPT,
            ),
        )
        Dialog.set_do_overwrite_confirmation(True)
        Filter = Gtk.FileFilter.new()
        Filter.set_name("Json files")
        Filter.add_pattern("*.json")
        Dialog.add_filter(Filter)
        Dialog.set_current_name("LBRY-GTK-Settings.json")
        Dialog.set_current_folder(str(pathlib.Path.home()))
        Ran = Dialog.run()

        while (
            Ran == Gtk.ResponseType.ACCEPT
            and not Dialog.get_filename().endswith(".json")
        ):
            Dialog.set_current_name(Dialog.get_current_name() + ".json")
            Ran = Dialog.run()

        if Ran == Gtk.ResponseType.ACCEPT and Dialog.get_filename().endswith(
            ".json"
        ):
            threading.Thread(
                target=self.ExportThread, args=([Dialog.get_filename()])
            ).start()
        Dialog.destroy()

    def ExportThread(self, Path):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            self.Session = json.load(File)
        self.LBRYSetting = settings.get(server=self.Session["Server"])
        Table = copy.deepcopy(self.LBRYSetting)
        Table["Session"] = self.Session
        Json = json.dumps(Table, indent=4)
        with open(Path, "w") as File:
            File.write(Json)
        GLib.idle_add(self.ExportUpdate, True)

    def ExportUpdate(self, Success):
        Message = "The export was not successful."
        if Success:
            Message = "The export was successful."
        Error(Message, self.Window)

    def on_Import_clicked(self, Widget):
        Dialog = Gtk.FileChooserDialog(
            "Import settings - LBRY-GTK",
            self.Window,
            Gtk.FileChooserAction.OPEN,
            (
                Gtk.STOCK_CANCEL,
                Gtk.ResponseType.CANCEL,
                Gtk.STOCK_OPEN,
                Gtk.ResponseType.ACCEPT,
            ),
        )
        Filter = Gtk.FileFilter.new()
        Filter.set_name("Json files")
        Filter.add_pattern("*.json")
        Dialog.add_filter(Filter)
        Dialog.set_current_folder(str(pathlib.Path.home()))
        if Dialog.run() == Gtk.ResponseType.ACCEPT:
            threading.Thread(
                target=self.ImportThread, args=([Dialog.get_filename()])
            ).start()
        Dialog.destroy()

    def ImportThread(self, Path):
        with open(Path, "r") as File:
            Table = json.load(File)
        self.LBRYSetting = copy.deepcopy(Table)
        del self.LBRYSetting["Session"]
        self.Session = copy.deepcopy(Table["Session"])
        GLib.idle_add(self.ImportUpdate, True)

    def ImportUpdate(self, Success):
        Message = "The export was not successful."
        if Success:
            Message = (
                "The export was successful. Click Save to save the settings."
            )
            self.ShowSettings(False)
        Error(Message, self.Window)

    def on_Save_clicked(self, Widget):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            self.Session = json.load(File)
        if self.Loaded:
            self.LBRYSetting = settings.get(server=self.Session["Server"])
            Settings = self.LBRYSetting["settings"]
            Shared = self.LBRYSetting["preferences"]["shared"]["value"]
            Local = self.LBRYSetting["preferences"]["local"]["value"]
            LBRYGTK = Shared["LBRY-GTK"]
            Shared["settings"]["show_mature"] = self.MatureContent.get_active()
            Settings["save_resolved_claims"] = self.SaveResolved.get_active()
            Settings["save_files"] = self.SaveFiles.get_active()
            Settings["save_blobs"] = self.SaveBlobs.get_active()
            Settings["download_dir"] = self.DownloadPath.get_filename()
            Settings[
                "blob_download_timeout"
            ] = self.BlobDownloadTimeout.get_value()
            Settings["download_timeout"] = self.DownloadTimeout.get_value()
            Settings["hub_timeout"] = self.HubTimeout.get_value()
            Settings["node_rpc_timeout"] = self.NodeRPCTimeout.get_value()
            Settings[
                "peer_connect_timeout"
            ] = self.PeerConnectTimeout.get_value()
            Local["tags"] = self.LocalTagger.Tags
            Shared["tags"] = self.SharedTagger.Tags

            for Setting in self.SettingFields:
                LBRYGTK[Setting] = self.GetValue(self.SettingFields[Setting])

            LBRYGTK["Order"] = self.Orderer.Get()

            LBRYGTK["NotTags"] = self.NotTagger.Tags

            try:
                LBRYGTK["CommentChannel"] = self.Channelser.Get()[0]
            except:
                pass

        for Setting in self.SessionFields:
            if Setting != "Server" and Setting != "Binary":
                self.Session[Setting] = self.GetValue(
                    self.SessionFields[Setting]
                )

        self.Session["NewServer"] = self.GetValue(self.SessionFields["Server"])
        self.Session["NewBinary"] = self.GetValue(self.SessionFields["Binary"])

        GLib.idle_add(self.SaveHelper)

    def SaveHelper(self):
        if (
            self.Session["Server"] != self.Session["NewServer"]
            or self.Session["Binary"] != self.Session["NewBinary"]
        ):
            Dialog = Gtk.MessageDialog(buttons=Gtk.ButtonsType.OK_CANCEL)
            Dialog.props.text = "Warning: changing Binary or Server only activates after a restart."
            Response = Dialog.run()
            Dialog.destroy()
            if Response == Gtk.ResponseType.OK:
                threading.Thread(target=self.SaveThread, args=()).start()
        else:
            threading.Thread(target=self.SaveThread, args=()).start()

    def SaveThread(self):
        if self.Loaded:
            JsonData = settings.set(
                self.LBRYSetting, server=self.Session["Server"]
            )
            if isinstance(JsonData, str):
                Error(JsonData, self.Window)
        with open(Places.ConfigDir + "Session.json", "w") as File:
            json.dump(self.Session, File)
        GLib.idle_add(self.UpdateWidth)
        GLib.idle_add(
            ChangeMenu,
            self.Window,
            self.Session["MenuType"],
            self.Session["MenuIcon"],
        )
        GLib.idle_add(
            ChangeMeta,
            self.Window,
            self.Session["EnableMetaService"],
        )

    def UpdateWidth(self):
        Pager = self.Window.get_children()[0].get_children()[0]
        Length = Pager.get_n_pages()
        for Index in range(Length):
            Page = Pager.get_nth_page(Index)
            Label = Pager.get_tab_label(Page)
            Children = Label.get_children()
            Children[0].set_size_request(self.Session["PageWidth"], -1)
            Children[1].set_hexpand(self.Session["PageExpand"])
