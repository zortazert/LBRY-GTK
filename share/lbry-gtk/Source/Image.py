################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, requests, os.path, threading
from gi.repository import Gtk, Gdk, GdkPixbuf, GLib
from PIL import Image

from Source import Places

Forbidden = ["<", ">", ":", '"', "\\", "/", "|", "?", "*"]


def Path(Url):
    Name = Url
    for Character in Forbidden:
        Name = Name.replace(Character, "_")
    return Places.CacheDir + Name


def GetUrlResolution(Url, Function, *Args):
    if Url != "" and Download(Url):
        GLib.idle_add(Function, *Args, Image.open(Path(Url)).size)
    else:
        GLib.idle_add(Function, *Args, (1, 1))


def GetPathResolution(Path, Function, *Args):
    if os.path.exists(Path):
        GLib.idle_add(Function, *Args, Image.open(Path).size)
    else:
        GLib.idle_add(Function, *Args, (1, 1))


def Download(Url):
    FilePath = Path(Url)
    if not os.path.exists(FilePath):
        try:
            Request = requests.get(Url, allow_redirects=True)
            open(FilePath, "wb").write(Request.content)
            Image.open(FilePath)
            return True
        except:
            pass
    else:
        try:
            Image.open(FilePath)
            return True
        except:
            pass
    return False


def FillPixbuf(Url, Pixbuf, Function="", *args):
    Pixbuf.fill(0x00000000)
    threading.Thread(
        target=FillPixbufDownload, args=([Url, Pixbuf, Function, *args])
    ).start()


GoodTypes = GdkPixbuf.Pixbuf.get_formats()
for Index in range(len(GoodTypes)):
    GoodTypes[Index] = GoodTypes[Index].get_name()


def FillPixbufDownload(Url, Pixbuf, Function, *args):
    FilePath = ""
    if Url != "" and Download(Url):
        FilePath = Path(Url)

    try:
        PILImage = Image.open(FilePath)
        if (
            not PILImage.format.lower() in GoodTypes
            or PILImage.format.lower() == "gif"
        ):
            PILImage = PILImage.convert("RGBA")
            PILImage.save(FilePath, "png")
    except:
        pass

    GLib.idle_add(FillPixbufFill, FilePath, Pixbuf, Function, *args)


def FillPixbufFill(FilePath, Pixbuf, Function, *args):
    Width, Height = Pixbuf.get_width(), Pixbuf.get_height()

    Screen = Gdk.Screen.get_default()
    IconTheme = Gtk.IconTheme.get_for_screen(Screen)
    Loaded = IconTheme.load_icon(
        "image-missing", min(Width, Height), Gtk.IconLookupFlags.FORCE_SIZE
    )
    if FilePath != "":
        Loaded = GdkPixbuf.Pixbuf.new_from_file_at_size(FilePath, Width, Height)

    NewWidth, NewHeight = Loaded.get_width(), Loaded.get_height()
    GdkPixbuf.Pixbuf.copy_area(
        Loaded,
        0,
        0,
        NewWidth,
        NewHeight,
        Pixbuf,
        (Width - NewWidth) / 2,
        (Height - NewHeight) / 2,
    )

    if Function != "":
        Function(*args)
