################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib

from flbry import channel, main

from Source import Places
from Source.Error import Error
from Source.DateTime import DateTime
from Source.Tag import Tag


class NewPublication:
    def __init__(self, *args):
        self.Builder, self.Window, self.Title = args

        self.NewPublication = self.Builder.get_object("NewPublication")
        self.Channel = self.Builder.get_object("Channel")
        self.Name = self.Builder.get_object("Name")
        self.PTitle = self.Builder.get_object("Title")
        self.File = self.Builder.get_object("File")
        self.Description = self.Builder.get_object("Description")
        self.ThumbnailType = self.Builder.get_object("ThumbnailType")
        self.LicenseType = self.Builder.get_object("LicenseType")
        self.ThumbnailUrl = self.Builder.get_object("ThumbnailUrl")
        self.ThumbnailFile = self.Builder.get_object("ThumbnailFile")
        self.TagBox = self.Builder.get_object("TagBox")
        self.LanguageBox = self.Builder.get_object("LanguageBox")
        self.Deposit = self.Builder.get_object("Deposit")
        self.Price = self.Builder.get_object("Price")
        self.LanguagesCombo = self.Builder.get_object("LanguagesCombo")
        self.License = self.Builder.get_object("License")
        self.LicenseLabel = self.Builder.get_object("LicenseLabel")
        self.LicenseModel = self.Builder.get_object("LicenseModel")
        self.LanguageModel = self.Builder.get_object("LanguageModel")
        self.DateTimeBox = self.Builder.get_object("DateTimeBox")

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "DateTime.glade")
        self.DateTimer = DateTime(Builder)
        Builder.connect_signals(self.DateTimer)
        self.DateTimeBox.pack_start(self.DateTimer.DateTime, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.Tagger = Tag(Builder, True)
        Builder.connect_signals(self.Tagger)
        self.TagBox.pack_start(self.Tagger.Tag, True, True, 0)

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Tag.glade")
        self.LanguageTagger = Tag(Builder, True)
        Builder.connect_signals(self.LanguageTagger)
        self.LanguageBox.pack_start(self.LanguageTagger.Tag, True, True, 0)
        self.LanguageTagger.Entry.hide()

        self.OSI = self.Builder.get_object("OSI")
        self.FSF = self.Builder.get_object("FSF")

        self.LicenseName = self.Builder.get_object("LicenseName")
        self.LicenseNameLabel = self.Builder.get_object("LicenseNameLabel")
        self.LicenseURL = self.Builder.get_object("LicenseURL")
        self.LicenseURLLabel = self.Builder.get_object("LicenseURLLabel")

        with open(Places.JsonDir + "Licenses.json") as Licenses:
            self.Licenses = json.load(Licenses)["licenses"]
        with open(Places.JsonDir + "Languages.json") as Languages:
            self.Languages = json.load(Languages)
        for License in self.Licenses:
            OSI, FSF = False, False
            try:
                OSI = License["isOsiApproved"]
            except:
                pass
            try:
                FSF = License["isFsfLibre"]
            except:
                pass

            self.LicenseModel.append(
                [License["name"], License["seeAlso"][0], OSI, FSF]
            )

        self.License.set_entry_text_column(0)

        for Language in self.Languages:
            LanguageText = Language[0] + "/" + Language[1]
            self.LanguagesCombo.append(None, LanguageText)
            self.LanguageModel.append([LanguageText])

    def ShowNewPublication(self):
        self.Channel.remove_all()
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)
        Channels = channel.channel_list(server=Session["Server"])
        for Channel in Channels:
            self.Channel.append(None, Channel[0])
        self.on_Clear_clicked()
        self.on_ThumbnailType_changed(self.ThumbnailType)
        self.on_LicenseType_changed(self.LicenseType)

        self.Replace("NewPublication")
        self.Title.set_text("New Publication")

    def on_LicenseEntry_changed(self, Widget):
        NewLicense = Widget.get_text()
        ID = 0
        for License in self.Licenses:
            if NewLicense == License["name"]:
                self.License.set_active(ID)
                break
            ID += 1
        Active = self.License.get_active()
        OSI, FSF = False, False
        if Active != -1:
            Path = Gtk.TreePath.new_from_indices([Active])
            Iter = self.LicenseModel.get_iter(Path)
            OSI = self.LicenseModel.get_value(Iter, 2)
            FSF = self.LicenseModel.get_value(Iter, 3)

        self.OSI.set_active(OSI)
        self.FSF.set_active(FSF)

    def on_LanguagesEntry_key_press_event(self, Widget, Event):
        self.LanguageTagger.on_Entry_key_press_event(Widget, Event)

    def on_LanguagesEntry_changed(self, Widget):
        self.LanguageTagger.Entry.set_text("")
        NewLanguage = Widget.get_text()
        ID = 0
        for Language in self.Languages:
            if NewLanguage == (Language[0] + "/" + Language[1]):
                self.LanguagesCombo.set_active(ID)
                self.LanguageTagger.Entry.set_text(NewLanguage)
                break
            ID += 1

    def NoToggleBefore(self, Widget):
        Widget.set_name(str(Widget.get_active()))

    def NoToggleAfter(self, Widget):
        Widget.set_active(Widget.get_name() == "True")

    def on_ThumbnailType_changed(self, Widget):
        if Widget.get_active() == 0:
            self.ThumbnailUrl.set_visible(False)
            self.ThumbnailFile.set_visible(True)
        else:
            self.ThumbnailUrl.set_visible(True)
            self.ThumbnailFile.set_visible(False)

    def on_LicenseType_changed(self, Widget):
        if Widget.get_active() == 0:
            self.License.set_visible(True)
            self.LicenseLabel.set_visible(True)
            self.LicenseName.set_visible(False)
            self.LicenseNameLabel.set_visible(False)
            self.LicenseURL.set_visible(False)
            self.LicenseURLLabel.set_visible(False)
        else:
            self.License.set_visible(False)
            self.LicenseLabel.set_visible(False)
            self.LicenseName.set_visible(True)
            self.LicenseNameLabel.set_visible(True)
            self.LicenseURL.set_visible(True)
            self.LicenseURLLabel.set_visible(True)

    def on_Clear_clicked(self, Widget=""):
        self.Channel.set_active(0)
        self.License.set_active(0)
        self.LanguagesCombo.set_active(0)
        self.ThumbnailType.set_active(0)
        self.Tagger.RemoveAll()
        self.LanguageTagger.RemoveAll()
        self.Name.set_text("")
        self.PTitle.set_text("")
        self.ThumbnailUrl.set_text("")
        self.Tagger.Entry.set_text("")
        self.Deposit.set_value(0.1)
        self.Price.set_value(0)
        self.File.set_filename("")
        self.ThumbnailFile.set_filename("")
        self.DateTimer.on_DateReset_clicked()
        self.DateTimer.DateUse.set_active(False)
        Buffer = self.Description.get_buffer()
        Buffer.set_text("")

    def on_Create_clicked(self, Widget):

        Active = self.Channel.get_active()
        Path = Gtk.TreePath.new_from_indices([Active])
        Model = self.Channel.get_model()
        Iter = Model.get_iter(Path)

        Channel = Model.get_value(Iter, 0)
        Name = self.Name.get_text()
        Title = self.PTitle.get_text()
        File = self.File.get_filename()

        Buffer = self.Description.get_buffer()
        Start = Buffer.get_start_iter()
        End = Buffer.get_end_iter()

        Description = Buffer.get_text(Start, End, True)

        ThumbnailFile = ""
        ThumbnailUrl = ""
        if self.ThumbnailType.get_active() == 0:
            ThumbnailFile = self.ThumbnailFile.get_filename()
        else:
            ThumbnailUrl = self.ThumbnailUrl.get_text()

        Tags = self.Tagger.Tags

        Deposit = self.Deposit.get_value()
        Price = self.Price.get_value()

        ReleaseTime = self.DateTimer.GetTime()

        Languages = self.LanguageTagger.Tags

        if self.LicenseType.get_active() == 0:
            Active = self.License.get_active()
            Path = Gtk.TreePath.new_from_indices([Active])
            Iter = self.LicenseModel.get_iter(Path)
            try:
                License = self.LicenseModel.get_value(Iter, 0)
                LicenseUrl = self.LicenseModel.get_value(Iter, 1)
            except:
                Error(
                    "You have to choose a license or set License Type to Custom.",
                    self.Window,
                )
                return
        else:
            License = self.LicenseName.get_text()
            LicenseUrl = self.LicenseURL.get_text()

        args = {
            "name": Name,
            "bid": Deposit,
            "file_path": File,
            "title": Title,
            "license": License,
            "license_url": LicenseUrl,
            "thumbnail_url": ThumbnailUrl,
            "thumbnail_file": ThumbnailFile,
            "channel_name": Channel,
            "description": Description,
            "tags": Tags,
            "fee_amount": Price,
            "languages": Languages,
            "release_time": ReleaseTime,
        }

        threading.Thread(target=self.UploadThread, args=([args])).start()

    def UploadThread(self, args):
        with open(Places.ConfigDir + "Session.json", "r") as File:
            Session = json.load(File)

        JsonData = main.publish(**args, server=Session["Server"])

        if isinstance(JsonData, str):
            Error(JsonData, self.Window)
            return
        Error("Publication successful.", self.Window)
