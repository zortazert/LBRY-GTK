################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import appdirs, os, sys, tempfile

AppName = "LBRY-GTK"
AppAuthor = "MorsMortium"

if sys.platform != "win32":
    Root = os.sep.join(
        os.path.abspath(os.path.dirname(sys.argv[0])).split(os.sep)[:-1]
    )
else:
    Root = os.path.abspath(os.path.dirname(sys.argv[0]))

ShareDir = Root + os.sep + "share" + os.sep

LBRYGTKDir = ShareDir + "lbry-gtk" + os.sep

ImageDir = (
    ShareDir
    + "icons"
    + os.sep
    + "hicolor"
    + os.sep
    + "scalable"
    + os.sep
    + "apps"
    + os.sep
)

TmpDir = tempfile.gettempdir() + os.sep + "LBRY-GTK" + os.sep

HelpDir = LBRYGTKDir + "Help" + os.sep

GladeDir = LBRYGTKDir + "Glade" + os.sep

JsonDir = LBRYGTKDir + "Json" + os.sep

ConfigDir = appdirs.user_config_dir(AppName, AppAuthor) + os.sep

CacheDir = appdirs.user_cache_dir(AppName, AppAuthor) + os.sep
