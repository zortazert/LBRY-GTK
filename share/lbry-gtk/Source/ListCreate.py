################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, Pango


def ColumnMaker(TreeView, Columns, Cells):
    for i in range(len(Columns)):
        if isinstance(Cells[i], Gtk.CellRendererPixbuf):
            NewColumn = Gtk.TreeViewColumn(Columns[i], Cells[i], pixbuf=i)
        else:
            if isinstance(Cells[i], Gtk.CellRendererText):
                NewColumn = Gtk.TreeViewColumn(Columns[i], Cells[i], text=i)
                if (
                    Cells[i].get_property("ellipsize")
                    == Pango.EllipsizeMode.END
                ):
                    NewColumn.set_expand(True)
            else:
                NewColumn = Gtk.TreeViewColumn(Columns[i], Cells[i], active=i)

        TreeView.append_column(NewColumn)


Cells = {
    "Default": Gtk.CellRendererText(),
    "Ellipsized": Gtk.CellRendererText(),
    "Edit": Gtk.CellRendererText(),
    "Pixbuf": Gtk.CellRendererPixbuf(),
    "Toggle": Gtk.CellRendererToggle(),
}

Cells["Ellipsized"].set_property("ellipsize", Pango.EllipsizeMode.END)
Cells["Edit"].set_property("editable", True)
Cells["Toggle"].set_activatable(True)

RowCells = [
    Cells["Pixbuf"],
    Cells["Default"],
    Cells["Default"],
    Cells["Toggle"],
    Cells["Default"],
    Cells["Ellipsized"],
    Cells["Default"],
]

DataCells = [Cells["Default"], Cells["Edit"]]


def MakeColumns(View, DataView):
    Columns = [
        "Thumbnail",
        "Confirmations",
        "Amount",
        "Is Tip",
        "Channel",
        "Title",
        "Type",
    ]
    ColumnMaker(View, Columns, RowCells)
    Columns = ["Name", "Value"]
    ColumnMaker(DataView, Columns, DataCells)
