################################################################################
# LBRY-GTK                                                                     #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

import gi, threading, json, queue, copy, time

gi.require_version("Gtk", "3.0")
from gi.repository import Gtk, GLib, GdkPixbuf

from flbry import search, wallet, list_files, meta

from Source import Image, Places, Settings, ListGridUtil
from Source.Error import Error
from Source.Box import Box

ClaimTypes = {"stream": 1, "channel": 2, "repost": 3, "collection": 4}

EmptyPixbuf = GdkPixbuf.Pixbuf().add_alpha(False, 0, 0, 0)
EmptyPixbuf.fill(0x00000000)


class List:
    ListPage, Loaded, Started = 0, 1, False

    def __init__(self, *args):
        (
            self.View,
            self.Window,
            self.Publicationer,
            self.Stores,
            self.WalletSpaceParts,
            self.MainSpace,
            self.Stater,
            self.Grid,
            self.Spaces,
            self.Text,
            self.Orderer,
            self.Channel,
            self.ClaimType,
            self.AnyTagger,
            self.AllTagger,
            self.NotTagger,
            self.ClaimIdTagger,
            self.ChannelIdTagger,
            self.DateTimer,
            self.Inequality,
            self.Advanced,
            self.StreamTyper,
            self.DateType,
            self.Title,
            self.AddPage,
        ) = args

        Builder = Gtk.Builder.new_from_file(Places.GladeDir + "Box.glade")
        Height = Builder.get_object("Type").get_preferred_height()
        self.TextHeight = Height.minimum_height
        Height = Builder.get_object("IsTip").get_preferred_height()
        self.CheckHeight = Height.minimum_height

        self.ListFunctions = {
            "Search": [search.simple],
            "Wallet": [
                ListGridUtil.BalanceHelper,
                wallet.history,
                self.WalletSpaceParts,
                self.Window,
            ],
            "File": [list_files.downloaded],
            "Inbox": [ListGridUtil.InboxHelper, meta.list],
        }
        self.Threads = []

    def UpdateMainSpace(self, LBRYSettings, Increase=0):
        self.Loaded += Increase
        for Thread in self.Threads:
            Thread[1].get()
        self.ListThread(LBRYSettings, *self.ListFunctions[self.ListButton])

    def Empty(self, Queue):
        self.Stores["Row"].clear()
        self.Grid.forall(Gtk.Widget.destroy)
        Queue.put(True)

    def Exit(self, ThreadQueue, UpdateMainSpaceQueue):
        self.Threads.remove([ThreadQueue, UpdateMainSpaceQueue])
        UpdateMainSpaceQueue.put(True)
        self.Started = True

    def ListThread(self, LBRYSettings, ListFunction, *args):
        ThreadQueue = queue.Queue()
        UpdateMainSpaceQueue = queue.Queue()
        self.Threads.append([ThreadQueue, UpdateMainSpaceQueue])
        Server = LBRYSettings["Session"]["Server"]
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        if self.ListPage == 0:
            EmptyQueue = queue.Queue()
            GLib.idle_add(self.Empty, EmptyQueue)
            EmptyQueue.get()
        for Index in range(self.ListPage, self.Loaded):
            Data = ListFunction(
                *args,
                **self.Data,
                page_size=int(LBRYGTKSettings["ContentPerLoading"]),
                page=Index + 1,
                server=Server,
            )
            if isinstance(Data, str):
                Error(Data, self.Window)
                self.ListPage = Index - 1
                self.Exit(ThreadQueue, UpdateMainSpaceQueue)
                return
            self.ListNumber += len(Data)
            Store = self.Stores["Row"]
            for Row in Data:
                try:
                    ThreadQueue.get(block=False)
                    self.Exit(ThreadQueue, UpdateMainSpaceQueue)
                    return
                except:
                    pass
                EmptyCopy = EmptyPixbuf.scale_simple(
                    int(LBRYGTKSettings["ThumbnailWidth"]),
                    int(LBRYGTKSettings["ThumbnailHeight"]),
                    GdkPixbuf.InterpType.NEAREST,
                )
                Args = [Row, EmptyCopy, self.MainSpace, self.List]
                if self.ListDisplay == 0:
                    GLib.idle_add(ListGridUtil.ListUpdate, *Args, Store)
                else:
                    GLib.idle_add(
                        ListGridUtil.GridUpdate,
                        *Args,
                        LBRYGTKSettings,
                        self.Publicationer,
                        self.Grid,
                        self.AddPage,
                        self.Stater,
                    )
        self.ListPage = self.Loaded
        self.Exit(ThreadQueue, UpdateMainSpaceQueue)

    def GenericLoaded(self, LBRYSettings):
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        ContentQueue = queue.Queue()
        ContentFunction = ListGridUtil.GridContent
        if self.ListDisplay == 0:
            ContentFunction = ListGridUtil.ListContent
        GLib.idle_add(
            ContentFunction,
            int(LBRYGTKSettings["ThumbnailHeight"]),
            int(LBRYGTKSettings["ThumbnailWidth"]),
            self.MainSpace,
            ContentQueue,
            LBRYGTKSettings,
            self.List,
            self.CheckHeight,
            self.TextHeight,
        )
        Content = ContentQueue.get()
        self.Loaded = Content // int(LBRYGTKSettings["ContentPerLoading"]) + 1
        if self.ListPage < self.Loaded:
            self.UpdateMainSpace(LBRYSettings)

    def ButtonThread(self, ButtonName, ListName, Title, Data={}, State=False):
        self.Started = False
        for Thread in self.Threads:
            Thread[0].put(True)
        while len(self.Threads) != 0:
            time.sleep(0.01)
        LBRYSettings = Settings.Get()
        if isinstance(LBRYSettings, str):
            Error(self.Window, LBRYSettings)
            return
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]
        self.ListDisplay = LBRYGTKSettings["ListDisplay"]
        self.Data = Data
        if not State:
            self.Stater.Save(
                self.ButtonThread, [ButtonName, ListName, Title, Data], Title
            )
        GLib.idle_add(
            self.ButtonUpdate, ButtonName, ListName, Title, Data, LBRYSettings
        )

    def ButtonUpdate(self, ButtonName, ListName, Title, Data, LBRYSettings):
        LBRYGTKSettings = LBRYSettings["preferences"]["shared"]["value"][
            "LBRY-GTK"
        ]

        # Initialization
        self.Advanced.set_expanded(False)
        self.Orderer.Set(LBRYGTKSettings["Order"])
        self.Text.set_text("")
        self.Channel.set_text("")
        self.ClaimType.set_active(0)
        self.Inequality.set_active(0)
        self.DateType.set_active(0)
        self.AnyTagger.RemoveAll()
        self.AllTagger.RemoveAll()
        self.NotTagger.RemoveAll()
        self.ClaimIdTagger.RemoveAll()
        self.ChannelIdTagger.RemoveAll()
        self.StreamTyper.RemoveAll()
        self.DateTimer.DateUse.set_active(False)
        self.DateTimer.on_DateReset_clicked()

        # Settings for Inbox
        if ButtonName == "Inbox":
            Data["meta_server"] = LBRYGTKSettings["MetaServer"]
            Data["auth_token"] = LBRYGTKSettings["AuthToken"]
        else:
            try:
                del Data["meta_server"]
                del Data["auth_token"]
            except:
                pass

        # Settings found from Data placed
        ShowMature = LBRYSettings["preferences"]["shared"]["value"]["settings"][
            "show_mature"
        ]
        if ListName == "Content":
            if "order_by" in Data:
                if Data["order_by"] == []:
                    self.Orderer.Set("")
                else:
                    self.Orderer.Set(Data["order_by"][0])
            else:
                Order = self.Orderer.Get()
                if Order:
                    Data["order_by"] = [Order]
            if not "not_tags" in Data:
                Data["not_tags"] = []
            if not ShowMature:
                Data["not_tags"].append("mature")
            if LBRYGTKSettings["NotTags"] != []:
                Data["not_tags"].extend(
                    copy.deepcopy(LBRYGTKSettings["NotTags"])
                )
        if "text" in Data:
            self.Text.set_text(Data["text"])
        if "channel" in Data:
            self.Channel.set_text(Data["channel"])
        if "claim_type" in Data:
            self.ClaimType.set_active(ClaimTypes[Data["claim_type"]])
        if "any_tags" in Data:
            self.AnyTagger.Append(Data["any_tags"])
        if "all_tags" in Data:
            self.AllTagger.Append(Data["all_tags"])
        if "not_tags" in Data:
            self.NotTagger.Append(Data["not_tags"])
        if "claim_ids" in Data:
            self.ClaimIdTagger.Append(Data["claim_ids"])
        if "channel_ids" in Data:
            self.ChannelIdTagger.Append(Data["channel_ids"])
        if "stream_types" in Data:
            self.StreamTyper.Append(Data["stream_types"])
        if "timestamp" in Data:
            Time = Data["timestamp"]
            if Time != "-1":
                self.DateType.set_active(2)
                self.DateTimer.DateUse.set_active(True)
                if Time.startswith("<="):
                    self.Inequality.set_active(3)
                    Time = Time[2:]
                elif Time.startswith(">="):
                    self.Inequality.set_active(4)
                    Time = Time[2:]
                elif Time.startswith("<"):
                    self.Inequality.set_active(0)
                    Time = Time[1:]
                elif Time.startswith(">"):
                    self.Inequality.set_active(1)
                    Time = Time[1:]
                else:
                    self.Inequality.set_active(2)
                self.DateTimer.SetTime(int(Time))

        self.ListButton = ButtonName
        self.List = ListName
        self.Title.set_text(Title)
        Parent = self.Grid.get_parent()
        if Parent:
            Parent.remove(self.Grid)
        Parent = self.View.get_parent()
        if Parent:
            Parent.remove(self.View)
        Space = self.Spaces[self.List]
        Item = self.Grid
        if self.ListDisplay == 0:
            Item = self.View
            for Index in range(1, 4):
                Item.get_column(Index).set_visible(self.List == "Wallet")
        Space.add(Item)
        self.Replace(self.List + "List")
        self.Publicationer.Queue.put(True)
        self.ListNumber = 0
        self.ListPage = 0
        threading.Thread(
            target=self.GenericLoaded, args=([LBRYSettings])
        ).start()
