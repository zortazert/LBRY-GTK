################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# This file will perform a simple search on the LBRY network.

import requests

from flbry import parse, error


def downloaded(page_size=5, page=1, server="http://localhost:5279"):
    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server,
            json={
                "method": "file_list",
                "params": {
                    "page_size": page_size,
                    "page": page,
                    "sort": "added_on",
                    "completed": True,
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return parse.files(json_data)
