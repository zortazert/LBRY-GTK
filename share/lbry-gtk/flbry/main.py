################################################################################
# LBRY-GTK Internals                                                           #
# Copyright (C) 2021-2022 MorsMortium and Other Contributors                   #
#                                                                              #
# This program is free software: you can redistribute it and/or modify         #
# it under the terms of the GNU General Public License as published by         #
# the Free Software Foundation, either version 3 of the License, or            #
# (at your option) any later version.                                          #
#                                                                              #
# This program is distributed in the hope that it will be useful,              #
# but WITHOUT ANY WARRANTY; without even the implied warranty of               #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                #
# GNU General Public License for more details.                                 #
#                                                                              #
# You should have received a copy of the GNU General Public License            #
# along with this program.  If not, see <http://www.gnu.org/licenses/>.        #
################################################################################

# This file will publish a selected file into LBRY Network.

import random, string, requests

from flbry import error


def publish(
    name="",
    bid=0.0001,
    file_path="",
    title="",
    license="",
    license_url="",
    thumbnail_url="",
    thumbnail_file="",
    channel_name="",
    description="",
    tags=[],
    fee_amount=0,
    fee_currency="LBC",
    fee_address="",
    validate_file=False,
    optimize_file=False,
    languages=[],
    locations=[],
    author="",
    release_time=-1,
    blocking=False,
    preview=False,
    no_thumbnail=False,
    funding_account_ids=[],
    channel_account_id=[],
    server="http://localhost:5279",
):

    # This function actually will upload the file.

    if name == "":
        chars = string.ascii_letters + "-_" + string.digits
        name = "".join(random.choice(chars) for x in range(70))

    if thumbnail_url == "" and thumbnail_file != "" and (not no_thumbnail):
        json_data = upload(file_path=thumbnail_file, no_thumbnail=True)
        if isinstance(json_data, str):
            return json_data
        thumbnail_url = json_data["outputs"][0]["permanent_url"].replace(
            "lbry://", "https://spee.ch/"
        )

    json = {
        "method": "publish",
        "params": {
            "name": name,
            "bid": str(bid),
            "file_path": file_path,
            "validate_file": validate_file,
            "optimize_file": optimize_file,
            "preview": preview,
            "blocking": blocking,
        },
    }

    if fee_currency != "" and fee_address != "" and fee_amount != "":
        json["params"]["fee_currency"] = fee_currency
        json["params"]["fee_address"] = fee_address
        json["params"]["fee_amount"] = fee_amount

    if funding_account_ids != []:
        json["params"]["funding_account_ids"] = funding_account_ids

    if channel_account_id != []:
        json["params"]["channel_account_id"] = channel_account_id

    if thumbnail_url != "":
        json["params"]["thumbnail_url"] = thumbnail_url

    if release_time != -1:
        json["params"]["release_time"] = release_time

    if channel_name != "":
        json["params"]["channel_name"] = channel_name

    if description != "":
        json["params"]["description"] = description

    if license_url != "":
        json["params"]["license_url"] = license_url

    if languages != []:
        json["params"]["languages"] = languages

    if locations != []:
        json["params"]["locations"] = locations

    if license != "":
        json["params"]["license"] = license

    if author != "":
        json["params"]["author"] = author

    if title != "":
        json["params"]["title"] = title

    if tags != []:
        json["params"]["tags"] = tags

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def get(
    uri="",
    file_name="",
    download_directory="",
    timeout=-1,
    save_file=None,
    wallet_id="",
    server="http://localhost:5279",
):

    json = {"method": "get", "params": {}}

    if download_directory != "":
        json["params"]["download_directory"] = download_directory

    if file_name != "":
        json["params"]["file_name"] = file_name

    if wallet_id != "":
        json["params"]["wallet_id"] = wallet_id

    if timeout != -1:
        json["params"]["timeout"] = timeout

    if save_file != None:
        json["params"]["save_file"] = save_file

    if uri != "":
        json["params"]["uri"] = uri

    try:
        json_data = {"error": {}}
        json_data = requests.post(server, json=json).json()
        json_data = json_data["result"]
        if "error" in json_data:
            json_data = {"error": {"message": json_data["error"], "code": -1}}
            raise Exception()
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data
