#####################################################################
#                                                                   #
#  THIS IS A SOURCE CODE FILE FROM A PROGRAM TO INTERACT WITH THE   #
# LBRY PROTOCOL ( lbry.com ). IT WILL USE THE LBRY SDK ( lbrynet )  #
# FROM THEIR REPOSITORY ( https://github.com/lbryio/lbry-sdk )      #
# WHICH I GONNA PRESENT TO YOU AS A BINARY. SINCE I DID NOT DEVELOP #
# IT AND I'M LAZY TO INTEGRATE IN A MORE SMART WAY. THE SOURCE CODE #
# OF THE SDK IS AVAILABLE IN THE REPOSITORY MENTIONED ABOVE.        #
#                                                                   #
#      ALL THE CODE IN THIS REPOSITORY INCLUDING THIS FILE IS       #
# (C) J.Y.Amihud and Other Contributors 2021. EXCEPT THE LBRY SDK.  #
# YOU CAN USE THIS FILE AND ANY OTHER FILE IN THIS REPOSITORY UNDER #
# THE TERMS OF GNU GENERAL PUBLIC LICENSE VERSION 3 OR ANY LATER    #
# VERSION. TO FIND THE FULL TEXT OF THE LICENSE GO TO THE GNU.ORG   #
# WEBSITE AT ( https://www.gnu.org/licenses/gpl-3.0.html ).         #
#                                                                   #
# THE LBRY SDK IS UNFORTUNATELY UNDER THE MIT LICENSE. IF YOU ARE   #
# NOT INTENDING TO USE MY CODE AND JUST THE SDK. YOU CAN FIND IT ON #
# THEIR OFFICIAL REPOSITORY ABOVE. THEIR LICENSE CHOICE DOES NOT    #
# SPREAD ONTO THIS PROJECT. DON'T GET A FALSE ASSUMPTION THAT SINCE #
# THEY USE A PUSH-OVER LICENSE, I GONNA DO THE SAME. I'M NOT.       #
#                                                                   #
# THE LICENSE CHOSEN FOR THIS PROJECT WILL PROTECT THE 4 ESSENTIAL  #
# FREEDOMS OF THE USER FURTHER, BY NOT ALLOWING ANY WHO TO CHANGE   #
# THE LICENSE AT WILL. SO NO PROPRIETARY SOFTWARE DEVELOPER COULD   #
# TAKE THIS CODE AND MAKE THEIR USER-SUBJUGATING SOFTWARE FROM IT.  #
#                                                                   #
#####################################################################

# Gets latest posts from subscriptions

import json, requests
from flbry import url, parse, error, search


def get_preferences(server="http://localhost:5279"):
    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server, json={"method": "preference_get"}
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def set_preferences(shared_preferences, server="http://localhost:5279"):
    try:
        json_data = {"error": {}}
        json_data = requests.post(
            server,
            json={
                "method": "preference_set",
                "params": {
                    "key": "shared",
                    "value": json.dumps(shared_preferences),
                },
            },
        ).json()
        json_data = json_data["result"]
    except Exception as e:
        return error.error(e, json_data["error"])

    return json_data


def follow_channel(channel, server="http://localhost:5279"):
    # Get the shared preferences
    preferences = get_preferences(server)

    if isinstance(preferences, str):
        return preferences

    try:
        shared_preferences = preferences["shared"]
    except:
        shared_preferences = {"value": {"subscriptions": []}}

    # If the channel is not in the subscriptions we add it
    if not channel in shared_preferences["value"]["subscriptions"]:
        shared_preferences["value"]["subscriptions"].append(channel)
        x = {"notificationsDisabled": True, "uri": channel}
        shared_preferences["value"]["following"].append(x)

        return set_preferences(shared_preferences, server)
    else:
        # Alert the user they're already subscribed
        return False


def unfollow_channel(channel, server="http://localhost:5279"):
    # Get the shared preferences
    preferences = get_preferences(server)

    if isinstance(preferences, str):
        return preferences

    try:
        shared_preferences = preferences["shared"]
    except:
        shared_preferences = {"value": {"subscriptions": []}}

    # If the channel is in the subscriptions we remove it
    if channel in shared_preferences["value"]["subscriptions"]:
        shared_preferences["value"]["subscriptions"].remove(channel)
        x = {"notificationsDisabled": True, "uri": channel}
        shared_preferences["value"]["following"].remove(x)

        return set_preferences(shared_preferences, server)
    else:
        # Alert the user they're not subscribed
        return False


def if_follow(channel, server="http://localhost:5279"):
    # Get the subscriptions
    preferences = get_preferences(server)

    if isinstance(preferences, str):
        return preferences

    try:
        subscriptions = preferences["shared"]["value"]["subscriptions"]
    except:
        return False

    return channel in subscriptions
